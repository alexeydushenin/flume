create table adreport(
  userid int,
  comment string,
  renders int,
  clicks int
  )
  clustered by (userid) into 5 buckets
  stored as orc

